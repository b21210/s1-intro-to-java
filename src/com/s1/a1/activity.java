package com.s1.a1;

import java.util.Scanner;

public class activity {
    public static void main(String[] args) {
        String firstName, lastName;
        Double firstSubject, secondSubject, thirdSubject;

        Scanner input = new Scanner(System.in);

        System.out.println("First Name:");
        firstName = input.next().trim();

        System.out.println("Last Name:");
        lastName = input.next().trim();

        System.out.println("First Subject Grade:");
        firstSubject = input.nextDouble();

        System.out.println("Second Subject Grade:");
        secondSubject = input.nextDouble();

        System.out.println("Third Subject Grade:");
        thirdSubject = input.nextDouble();

        double average = (firstSubject + secondSubject + thirdSubject)/3;

        System.out.println("Good day, " + firstName + " " + lastName);
        System.out.println("Your grade average is: " + (int)(average));

    }
}
